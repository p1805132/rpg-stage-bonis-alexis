﻿		 										 									 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ennemyAI : MonoBehaviour {

    // Distance between enemy and player
    private float Distance;

    // Distance between enemy and base position
    private float DistanceBase;
    private Vector3 basePositions;

    // Enemy target
    public Transform Target;

    //Chase Range
    public float chaseRange = 10;

    // Attack range
    public float attackRange = 2.2f;

    // Attack cooldown
    public float attackRepeatTime = 1;
    private float attackTime;

    // Amount of damage
    public float TheDammage;

    // Navigation agent
    private UnityEngine.AI.NavMeshAgent agent;

    // Enemy's animation
    private Animation animations;

    // Enemy's health
    public float enemyHealth;
    private bool isDead = false;

    // Enemy's loot
    public GameObject[] loots;

    void Start () {
        agent = gameObject.GetComponent< UnityEngine.AI.NavMeshAgent>();
        animations = gameObject.GetComponent<Animation>();
        attackTime = Time.time;
        basePositions = transform.position;
    }



    void Update () {

        if (!isDead)
        {
            Target = GameObject.Find("Player").transform;

            Distance = Vector3.Distance(Target.position, transform.position);

            DistanceBase = Vector3.Distance(basePositions, transform.position);

            if (Distance > chaseRange && DistanceBase <= 1)
            {
                idle();
            }

            if (Distance < chaseRange && Distance > attackRange)
            {
                chase();
            }

            if (Distance < attackRange)
            {
                attack();
            }

            if (Distance > chaseRange && DistanceBase > 1) {
                BackBase();
            }

        }
    }

    void chase()
    {
        animations.Play("Walk");
        agent.destination = Target.position;
    }

    void attack()
    {
        agent.destination = transform.position;

        if (Time.time > attackTime) {
            animations.Play("Attack");
            Target.GetComponent< PlayerInventory>().ApplyDamage(TheDammage);
            Debug.Log("L'ennemi a envoyÃ© " + TheDammage + " points de dÃ©gÃ¢ts");
            attackTime = Time.time + attackRepeatTime;
        }
    }

    // idle
    void idle()
    {
        animations.Play("Idle");
    }

    public void ApplyDammage(float TheDammage)
    {
        if (!isDead)
        {
            enemyHealth = enemyHealth - TheDammage;
            animations.Play("Hit");
            print(gameObject.name + "a subit " + TheDammage + " points de dÃ©gÃ¢ts.");

            if(enemyHealth <= 0)
            {
                Dead();
            }
        }
    }

    public void BackBase() {
        animations.Play("Walk");
        agent.destination = basePositions;
    }

    public void Dead()
    {
        gameObject.GetComponent< CapsuleCollider>().enabled = false;
        isDead = true;
        animations.Play("Die");

        int randomNumber = Random.Range(0, loots.Length);
        GameObject finalLoot = loots[randomNumber];
        Instantiate(finalLoot, transform.position, transform.rotation);

        Destroy(transform.gameObject, 5);
    }
}